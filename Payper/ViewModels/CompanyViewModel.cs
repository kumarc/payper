﻿using Payper.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Payper.ViewModels
{
    public class CompanyViewModel
    {
        public string HeaderText { get; set; }

        //[Required]
        public Company company { get; set; }
        //[Required]
        public Bank bank { get; set; }

    }
}