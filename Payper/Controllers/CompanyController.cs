﻿using Payper.Models;
using Payper.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Payper.Controllers
{
    public class CompanyController : Controller
    {
        private PayperEntities db = new PayperEntities();
        // GET: Company
        [HttpGet]
        public ActionResult Index()
        {
            var model = new CompanyViewModel();
            model.HeaderText = "Company Details";

            return View(model);
        }

        [HttpPost]
        public ActionResult Index(CompanyViewModel viewModel)
        {
            Company comp = new Company();
            comp.Name = viewModel.company.Name;
            comp.ShortName = viewModel.company.ShortName;
            comp.Address = viewModel.company.ShortName;

            Bank bank = new Bank();
            bank.Name = viewModel.bank.Name;
            bank.Branch = viewModel.bank.Branch;
            bank.AccountNumber = viewModel.bank.AccountNumber;
            bank.AccountName = viewModel.bank.AccountName;
            
            comp.Banks.Add(bank);

            if (ModelState.IsValid)
            {
                try
                {
                    db.Companies.Add(comp);
                    db.SaveChanges();
                }
                catch (Exception)
                {
                    
                    throw;
                }
                
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");

        }
        // GET: Company/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Company/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Company/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Company/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Company/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Company/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Company/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}
